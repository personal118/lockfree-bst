#include <iostream>
#include <algorithm>

using namespace std;

namespace Sequential {

	const int inf1 = INT_MAX - 1;
	const int inf2 = INT_MAX;

	struct treeNode
	{
		int data;
		bool isLeaf;
		treeNode *left;
		treeNode *right;

		treeNode(int d, bool b, treeNode *l = nullptr, treeNode *r = nullptr) : data(d), isLeaf(b), left(l), right(r)
		{
		}
	};

	struct searchResult
	{
		treeNode *p;
		treeNode *l;

		searchResult(treeNode *pp, treeNode *ll) : p(pp), l(ll)
		{
		}
	};

	class BST
	{
	private:
		treeNode *root;

		void preorder(treeNode *);
		struct searchResult *search(int);
	public:

		BST()
		{
			treeNode *rleft = new treeNode(inf1, true);
			treeNode *rright = new treeNode(inf2, true);
			root = new treeNode(inf2, false, rleft, rright);
		};

		void print_preorder();
		bool insert(int);
		bool find(int);
	};


	void BST::print_preorder()
	{
		preorder(root);
		cout << endl;
	}

	void BST::preorder(treeNode *p)
	{
		if (!p)
			return;

		if (p->isLeaf)
			cout << " " << p->data << " ";
		else
		{
			treeNode *l = p->left;
			treeNode *r = p->right;

			if (l)
				preorder(l);
			if (r)
				preorder(r);
		}
	}

	searchResult *BST::search(int k)
	{
		treeNode *p;
		treeNode *l;

		l = root;

		do
		{
			p = l;

			if (k < l->data)
			{
				l = p->left;
			}
			else
			{
				l = p->right;
			}

		} while (!(l->isLeaf));

		return new searchResult(p, l);
	}

	bool BST::find(int k) {
		return this->search(k)->l->data == k;
	}

	bool BST::insert(int k)
	{
		searchResult *s = search(k);

		treeNode *newInternal;
		newInternal = s->l;
		if (newInternal->data == k)
		{
			return false;
		}

		treeNode *newNode = new treeNode(k, true);
		treeNode *newSibling = new treeNode(newInternal->data, true);

		newInternal->data = max(k, newInternal->data);
		newInternal->isLeaf = false;

		if (k < newInternal->data)
		{
			newInternal->left = newNode;
			newInternal->right = newSibling;
		}
		else
		{
			newInternal->left = newSibling;
			newInternal->right = newNode;
		}

		return true;
	}
}
