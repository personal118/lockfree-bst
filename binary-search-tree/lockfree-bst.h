#pragma once

#include <iostream>
#include <atomic>
#include <climits>
#include <algorithm>

using namespace std;

namespace Lockfree {

	const int inf1 = INT_MAX - 1;
	const int inf2 = INT_MAX;

	struct infoRecord;
	struct updateRecord {
		bool isDirty;
		infoRecord *info;
	};

	struct treeNode
	{
		int data;
		bool isLeaf;
		atomic<treeNode*> left;
		atomic<treeNode*> right;
		atomic<updateRecord> update;

		treeNode(int d, bool b, treeNode *l = nullptr, treeNode *r = nullptr, updateRecord *u = nullptr) : data(d), isLeaf(b) 
		{
			left.store(l);
			right.store(r);
			if (u) {
				update.store(*u);
			}else{
				updateRecord emptyUR = {false, nullptr};
				update.store(emptyUR);
			}
		}
	};

	struct infoRecord {
		treeNode* parent;
		treeNode* leaf;
		treeNode* subtree;

		infoRecord(treeNode* p = nullptr, treeNode* l = nullptr, treeNode* s = nullptr): parent(p),  leaf(l), subtree(s)
		{
		}
	};

	struct searchResult
	{
		atomic<treeNode*> p;
		atomic<treeNode*> l;
		atomic<updateRecord> pupdate;

		searchResult(treeNode *pp, treeNode *ll, updateRecord uu)
		{
			p.store(pp);
			l.store(ll);
			pupdate.store(uu);
		}
	};

	class BST
	{
	private:
		atomic<treeNode*> root;

		void preorder(treeNode *);
		struct searchResult *search(int);
		void helpInsert(infoRecord *op);
		bool CASChild(treeNode *parent, treeNode *oldNode, treeNode *newNode);
	public:

		BST()
		{
			treeNode *rleft = new treeNode(inf1, true);
			treeNode *rright = new treeNode(inf2, true);
			root = new treeNode(inf2, false, rleft, rright);
		};

		void print_preorder();
		bool insert(int);
		bool find(int);
	};


	void BST::print_preorder()
	{
		preorder(root);
		cout << endl;
	}

	void BST::preorder(treeNode *p)
	{
		if (!p)
			return;

		if (p->isLeaf)
			cout << " " << p->data << " ";
		else
		{
			treeNode *l = p->left;
			treeNode *r = p->right;

			if (l)
				preorder(l);
			if (r)
				preorder(r);
		}
	}

	searchResult *BST::search(int k)
	{
		atomic<treeNode*> p;
		atomic<treeNode*> l;
		atomic<updateRecord> pupdate;

		l.store(root);

		do
		{
			p.store(l.load());
			pupdate.store(l.load()->update);

			if (k < l.load()->data)
			{
				l.store(p.load()->left);
			}
			else
			{
				l.store(p.load()->right);
			}

		} while (!(l.load()->isLeaf));

		return new searchResult(p, l, pupdate);
	}

	bool BST::find(int k) {
		return this->search(k)->l.load()->data == k;
	}

	bool BST::insert(int k)
	{
		treeNode *p;
		treeNode *newInternal;
		treeNode *l;
		treeNode *newSibling;
		treeNode *newNode;
		infoRecord * op;
		updateRecord pupdate;

		newNode = new treeNode(k, true);

		while (true) {
			searchResult *s = search(k);

			p = s->p.load();
			l = s->l.load();
			pupdate = s->pupdate.load();

			if (l->data == k)
				return false;

			if (pupdate.isDirty) {
				helpInsert(pupdate.info);
				continue;
			}

			newSibling = new treeNode(l->data, true);

			if (newNode->data < newSibling->data)
				newInternal = new treeNode(max(k, l->data), false, newNode, newSibling);
			else
				newInternal = new treeNode(max(k, l->data), false, newSibling, newNode);

			op = new infoRecord(p, l, newInternal);
			updateRecord dirty = {true, op};

			bool iflag = p->update.compare_exchange_strong(pupdate, dirty);
			if (iflag) {
				helpInsert(op);
				return true;
			}
			else
				helpInsert(pupdate.info);
			}
	}

	void BST::helpInsert(infoRecord *op)
	{
		treeNode *p, *l, *s;
		p = op->parent;
		l = op->leaf;
		s = op->subtree;

		updateRecord dirty = { true, op },
			clean = { false, op };

		CASChild(p, l, s);
		op->parent->update.compare_exchange_strong(dirty, clean);
	}

	bool BST::CASChild(treeNode *parent, treeNode *oldNode, treeNode *newNode)
	{
		atomic<treeNode*> *childToChange;
		if (newNode->data < parent->data)
			childToChange = &(parent->left);
		else
			childToChange = &(parent->right);
		
		return childToChange->compare_exchange_strong(oldNode, newNode);
	}
}

