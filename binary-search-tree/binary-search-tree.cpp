﻿// binary-search-tree.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include <atomic>
#include <vector>
#include <climits>
#include <random>
#include <algorithm>
#include "sequential-bst.h"
#include "lockfree-bst.h"
#include <vector>
#include <string>
#include <functional>
#include <thread>
#include <cassert>

//Number of nodes inserted in single-threaded mode
#define ST_N 100000

//Number of nodes inserted in multi-threaded mode (for up to 32 threads)
#define MT_N 100000         //Warning: This number is inserted for each thread

//Utility macros to print debug messages during the tests
#define DEBUG_ENABLED true
#define DEBUG(message) if(DEBUG_ENABLED) std::cout << message << std::endl;

using namespace std; 

/*!
 * Launch a single threaded test on the given structure.
 * \param T The type of the structure.
 * \param name The name of the structure being tested.
 */
template<typename T>
void testST(string name) {
	std::cout << "Test single-threaded (with " << ST_N << " elements) " << name << std::endl;

	T tree;

	std::mt19937_64 engine(time(NULL));

	//Note: The max() value cannot be handled by all data structure
	std::uniform_int_distribution<int> distribution(0, std::numeric_limits<int>::max() - 1);
	auto generator = std::bind(distribution, engine);

	DEBUG("Insert N random numbers in the tree");

	for (unsigned int i = 0; i < ST_N; ++i) {
		int number = generator();

		if (tree.find(number)) {
			assert(!tree.insert(number));
			assert(tree.find(number));
		}
		else {
			assert(tree.insert(number));
			assert(tree.find(number));
		}
	}

	std::cout << "Test passed successfully" << std::endl;
}


template<typename T, unsigned int Threads>
void testMT(string name) {
	T* tree = new T();

	std::cout << "Test multi-threaded (with " << ST_N << " elements) " << name << std::endl;

	std::vector<std::thread> pool;

	DEBUG("Make some operations")

		for (unsigned int i = 0; i < Threads; ++i) {
			pool.push_back(std::thread([&tree, i]() {
				std::mt19937_64 engine(time(0) + i);

				//Note: The max() value cannot be handled by all data structure
				std::uniform_int_distribution<int> distribution(0, std::numeric_limits<int>::max() - 1);
				auto generator = std::bind(distribution, engine);

				for (int n = 0; n < 10000; ++n) {
					auto value = generator();

					tree->insert(value);
					assert(tree->find(value));
				}
			}));
		}

	for_each(pool.begin(), pool.end(), [](std::thread& t) {t.join(); });

	std::cout << "Test with " << Threads << " threads passed succesfully" << std::endl;
}



int main()
{
	testST<Sequential::BST>("Sequential");
	testST<Lockfree::BST>("Lockfree");
	testMT<Lockfree::BST, 2>("Lockfree");
	testMT<Lockfree::BST, 4>("Lockfree");
	testMT<Lockfree::BST, 8>("Lockfree");
	testMT<Lockfree::BST, 16>("Lockfree");
	testMT<Lockfree::BST, 32>("Lockfree");
}
